module.exports = {
    get Adapter () {
        return require('./Adapter');
    },

    get combiner () {
        return require('./combiner/');
    },

    get Error () {
        return require('./Error');
    },

    get Manager () {
        return require('./Manager');
    }
};
